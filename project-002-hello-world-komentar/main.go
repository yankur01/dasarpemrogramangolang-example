package main

import "fmt"

func main(){
	//ini adalah contoh komentar 1 baris
	fmt.Println("Hello world dengan komentar")
	//bila di berikan komentar seperti ini maka
	//tidak akan di eksekusi oleh program

	/*
	ini adalah komentar
	multipline
	tidak perlu menambahkan // setiap baris
	 */

}


